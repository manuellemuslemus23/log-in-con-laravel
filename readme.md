# Log In con laravel y Mysql

Inicio de sesion utilizando el Laravel framework y base de datos en Mysql

### Requisitos

- php 7.1 >
- mysql
- apache2
- Composer

1) Crear base de datos en Mysql

2) buscar archivo .env y modificar los campos :

(DB_DATABASE, DB_USERNAME, DB_PASSWORD)
 
 ##### Ejemplo:

    
    DB_DATABASE=nombre_de_la_base_de_datos
    DB_USERNAME=usuario_de_la_base_de_datos
    DB_PASSWORD=mcontrasena_de_usuario_de_base_de_datos

3.Posicionarse dentro de la carpeta, abrir un terminal y ejecutar

   
```bash
   php artisan migrate
```
Con este comando se ejecutaran las migraciones en laravel, que a su ves crearan las tablas en la base de datos

4.ejecutar en la terminal
```bash
   php artisan serve
```

